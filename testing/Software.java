package ar.edu.ub.testing;

public class Software
{
	public Software(String name, int sizeMB)
	{
		this.m_name = name;
		this.m_size = sizeMB;
	}
	
	public String name()
	{
		return this.m_name;
	}
	
	public int size()
	{
		return this.m_size;
	}
	
	private String m_name;
	private int m_size;
}
