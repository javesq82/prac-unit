package ar.edu.ub.testing.tests;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import ar.edu.ub.testing.CPU;
import ar.edu.ub.testing.Computer;
import ar.edu.ub.testing.Disk;
import ar.edu.ub.testing.OperatingSystem;
import ar.edu.ub.testing.RAM;
import ar.edu.ub.testing.StandardOutput;

public class ComputerTest {

    @Test
    public void testValidCPU() {
        CPU cpu = new CPU("Lenovo", 900);
        Computer com = new Computer();
        com.setCPU(cpu);
        assertEquals(cpu, com.cpu());
    }

    @Test
    public void testNullCPU() {
        Computer com = new Computer();
        com.setCPU(null);
        assertNull(com.cpu());
    }

    @Test
    public void testValidRAM() {
        RAM ram = new RAM("AMD", 800, 2000);
        Computer com = new Computer();
        com.setRAM(ram);
        assertEquals(ram, com.ram());
    }

    @Test
    public void testInvalidRAM() {
        RAM ram = new RAM(null, -99999999, 0);
        Computer com = new Computer();
        com.setRAM(ram);
        assertEquals(ram, com.ram());
        
    }

    @Test
    public void testValidDisk() {
        Disk disk = new Disk("Sony", 800, 900);
        Computer com = new Computer();
        com.setDisk(disk);
        assertEquals(disk, com.disk());
    }

    @Test
    public void testNullDisk() {
        Disk disk = new Disk("Sony", 800, 900);
        Computer com = new Computer();
        com.setDisk(null);
        assertNull(com.disk());
    }

    @Test
    public void testValidOperatingSystem() {
        OperatingSystem os = new OperatingSystem("Windows", 700);
        Computer com = new Computer();
        assertEquals(os, com.os());
    } 

    @Test
    public void testValidInstallOS() {
        Computer com = new Computer();
        OperatingSystem os = new OperatingSystem("Word", 300);
        os.setOuput(new StandardOutput());
        com.setRAM(new RAM("aaa", 1000, 900));
        com.setDisk(new Disk("ppp", 1000, 900));
        assertTrue(com.installOS(os));

    }

    @Test
    public void testNullInstallOS() {
        Computer com = new Computer();
        OperatingSystem os = new OperatingSystem("Word", 300);
        os.setOuput(new StandardOutput());
        com.setRAM(new RAM("aaa", 1000, 900));
        com.setDisk(new Disk("ppp", 1000, 900));
        assertNull(com.installOS(null));

    }

    @Test
    public void testInvalidInstallOS() {
        Computer com = new Computer();
        OperatingSystem os = new OperatingSystem("Word", 300);
        os.setOuput(new StandardOutput());
        com.setRAM(new RAM("aaa", 1000, 900));
        com.setDisk(new Disk("ppp", 1, 1));
        assertFalse(com.installOS(os));

    }


    
}
