package ar.edu.ub.testing.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.Test;

import ar.edu.ub.testing.Software;

public class SoftwareTest {

    @Test
    public void testValidName() {
        Software soft = new Software("Office", 500);
        assertEquals("Office", soft.name());

    }

    @Test
    public void testNullName() {
        Software soft = new Software(null, 500);
        assertNull(soft.name());
    }

    @Test
    public void testValidSize() {
        Software soft = new Software("Office", 500);
        assertEquals(500, soft.size());
    }

    @Test
    public void testNegativeSize() {
        Software soft = new Software("Office", -100);
        assertEquals(-100, soft.size());
    }
    
}
