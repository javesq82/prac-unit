package ar.edu.ub.testing.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import ar.edu.ub.testing.Disk;
import ar.edu.ub.testing.Software;

public class DiskTest {

    private ArrayList<Software> m_openSoftware;

    @Test
    public void testValidSize() {
        Disk disk = new Disk("Sony", 900, 800);
        assertEquals(800, disk.size());
    }

    @Test
    public void testInvalidSize() {
        Disk disk = new Disk("HP", 500, 2000);
        assertNotEquals(900, disk.size());
    }

    @Test
    public void testSpeed() {
        Disk disk = new Disk("HP", 600, 1000);
        assertEquals(600, disk.speed());
    }

    @Test
    public void testNegativeSpeed() {
        Disk disk = new Disk("HP", -500,500);
        assertEquals(-500, disk.speed());

    }

    @Test
    public void testValidTaken() {
        Software soft = new Software("Linux", 700);
        Disk disk = new Disk("HP", 800, 1000);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        assertEquals(0, disk.taken());
    }

    @Test
    public void testValidInstall() {
        Software soft = new Software("Windows", 800);
        Disk disk = new Disk("Sony", 800, 1200);
        this.m_openSoftware = new ArrayList<Software>();
        disk.taken();
        m_openSoftware.add(soft);
        disk.sleep(soft.size()/disk.speed());
        assertTrue(disk.install(soft));
    }

    @Test
    public void testInvalidInstall() {
        Software soft = new Software("Mac", 800);
        Disk disk = new Disk("Samsung", 800, 100);
        this.m_openSoftware = new ArrayList<Software>();
        disk.taken();
        m_openSoftware.add(soft);
        disk.sleep(soft.size()/disk.speed());
        assertFalse(disk.install(soft));

    }

    @Test
    public void testValidUninstall() {
        Software soft = new Software("Linux", 600);
        Disk disk = new Disk("Sony", 1000, 1000);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        disk.uninstall("Linux");
        disk.sleep(soft.size()/disk.speed());
        m_openSoftware.remove(soft);
        assertTrue(disk.uninstall(soft.name()));

    }

    @Test
    public void testInvalidUninstall() {
        Software soft = new Software("", 0);
        Disk disk = new Disk("Sony", 1000, 1000);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        disk.sleep(soft.size()/disk.speed());
        m_openSoftware.remove(soft);
        assertFalse(disk.uninstall(soft.name()));

    }

    @Test
    public void testValidRetrieve() {
        Software soft = new Software("Office", 600);
        Disk disk = new Disk("Phillips", 1000, 1000);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        disk.sleep(soft.size()/disk.speed());
        assertEquals(disk.retrieve("Office"), disk.retrieve(soft.name()));

    }
    @Test
    public void testInvalidRetrieve() {
        Software soft = new Software("", 400);
        Disk disk = new Disk("LG", 1600, 800);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        disk.sleep(soft.size()/disk.speed());
        assertNull(disk.retrieve(""));

    }

    


    
}
