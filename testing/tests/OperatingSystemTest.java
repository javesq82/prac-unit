import ar.edu.ub.testing.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestStandardOutput extends StandardOutput
{
    public void print(String message)
    {
        this.m_lastMessage = message;
    }

    public String lastMessage()
    {
        return this.m_lastMessage;
    }

    private String m_lastMessage;
}

public class OperatingSystemTest
{
    @BeforeEach
    void setUp()
    {
        this.m_pc = new Computer();
        this.m_pc.setCPU(new CPU("Intel Pentium II", 266));
        this.m_pc.setRAM(new RAM("Kingston", 133, 256));
        this.m_pc.setDisk(new Disk("Seagate", 500, 40000));
    }

    @Test
    void testOperatingSystemConstructorStandardOutputNotNull()
    {
        OperatingSystem os = new OperatingSystem("Windows 95", 300);
        assertNotNull(os.output());
    }

    @Test
    void testOperatingSystemSetStandardOutput()
    {
        TestStandardOutput tso = new TestStandardOutput();
        OperatingSystem os = new OperatingSystem("Windows 95", 300);
        os.setOuput(tso);
        assertEquals(tso, os.output());
    }

    @Test
    void testOperatingSystemSetStandardOutputNull()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(null);
        assertNull(os.output());
    }

    @Test
    void testOperatingSystemInstall()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        Software s = new Software("Word 97", 100);
        assertTrue(os.install(s));
        assertEquals("Successfully installed Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertEquals(s, this.m_pc.disk().retrieve("Word 97"));
    }

    @Test
    void testOperatingSystemInstallNullSoftware()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        assertFalse(os.install(null));
        assertEquals("Install: software to be installed not provided", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallUnnamedSoftware()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        assertFalse(os.install(new Software("", 100)));
        assertEquals("Install: cannot install unidentified software", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallZeroSizedSoftware()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        assertFalse(os.install(new Software("Word 97", 0)));
        assertEquals("Install: cannot install software that doesn't have a size", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallZeroNegativeSoftware()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        assertFalse(os.install(new Software("Word 97", -100)));
        assertEquals("Install: cannot install software that doesn't has a negative size", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallTwice()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        Software s = new Software("Word 97", 100);
        assertTrue(os.install(s));
        assertEquals("Successfully installed Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertEquals(s, this.m_pc.disk().retrieve("Word 97"));
        assertFalse(os.install(s));
        assertEquals("Word 97 is already installed", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallUninstall()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        Software s = new Software("Word 97", 100);
        assertTrue(os.install(s));
        assertEquals("Successfully installed Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertEquals(s, this.m_pc.disk().retrieve("Word 97"));
        assertTrue(os.uninstall(s.name()));
        assertEquals("Successfully uninstalled Word 97", ((TestStandardOutput)os.output()).lastMessage());
    }

    @Test
    void testOperatingSystemInstallUninstallReinstall()
    {
        OperatingSystem os = new OperatingSystem("Windows 98", 500);
        os.setOuput(new TestStandardOutput());
        os.manage(this.m_pc);

        Software s = new Software("Word 97", 100);
        assertTrue(os.install(s));
        assertEquals("Successfully installed Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertEquals(s, this.m_pc.disk().retrieve("Word 97"));
        assertTrue(os.uninstall(s.name()));
        assertEquals("Successfully uninstalled Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertTrue(os.install(s));
        assertEquals("Successfully installed Word 97", ((TestStandardOutput)os.output()).lastMessage());
        assertEquals(s, this.m_pc.disk().retrieve("Word 97"));
    }

    private Computer m_pc;
}
