package ar.edu.ub.testing.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import ar.edu.ub.testing.RAM;
import ar.edu.ub.testing.Software;

public class RAMTest {

    private ArrayList<Software> m_openSoftware;
    

    @Test
    public void testValidSpeed() {
        RAM ram = new RAM("intel", 300, 500);
        assertEquals(300, ram.speed());
        
    }

    @Test
    public void testInvalidSpeed() {
        RAM ram = new RAM("AMD", 150, 300);
        assertNotEquals(80, ram.speed());
    }

    @Test
    public void testSizeNullName() {
        RAM ram = new RAM(null, 500, 500);
        assertNull(ram.name());
    }

    @Test
    public void testValidSize() {
        RAM ram = new RAM("AMD", 500, 900);
        assertEquals(900, ram.size());
    }

    @Test
    public void testValidTaken() {
        Software soft = new Software("Windows", 600);
        RAM ram = new RAM("AMD", 150, 100);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        assertEquals(0, ram.taken());
    }

    @Test
    public void testIsNotValidTaken() {
        Software soft = new Software("Windows", 600);
        RAM ram = new RAM("AMD", 150, 100);
        this.m_openSoftware = new ArrayList<Software>();
        m_openSoftware.add(soft);
        assertNotEquals(100, ram.taken());
    }
    
    @Test
    public void testValidClose() {
        this.m_openSoftware = new ArrayList<Software>();
        Software soft = new Software("Mac", 400);
        RAM ram = new RAM("intel", 150, 100);
        m_openSoftware.add(soft);
        ram.sleep(soft.size()/ram.speed());
        m_openSoftware.remove(soft);
        assertTrue(ram.close("Mac"));

    }

    @Test
    public void testIsNotValidClose() {
        this.m_openSoftware = new ArrayList<Software>();
        Software soft = new Software("Windows", 100);
        RAM ram = new RAM("intel", 150, 100);
        m_openSoftware.add(soft);
        assertFalse(ram.close("Linux"));

    }
}
