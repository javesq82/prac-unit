package ar.edu.ub.testing;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Main
{
	public static JSONObject loadDataFile(String fileName)
	{
		try
		{
			return new JSONObject(new String(Files.readAllBytes(FileSystems.getDefault().getPath(fileName))));
		}
		catch (IOException e)
		{
		}
		catch (JSONException e)
		{
		}
		
		return null;
	}
	
	public static Computer setupPC(JSONObject withData)
	{
		JSONObject computerData = JSONUtils.readObjectProperty("computer", withData, null);
		if (computerData != null)
		{
			Computer computer = new Computer();
			
			JSONObject cpuData = JSONUtils.readObjectProperty("cpu", computerData, null);
			if(cpuData != null)
			{
				computer.setCPU(new CPU(JSONUtils.readProperty("name", cpuData, "UNNAMED CPU"), JSONUtils.readProperty("speed_mhz", cpuData, 1600)));
			}
			
			JSONObject ramData = JSONUtils.readObjectProperty("ram", computerData, null);
			if(cpuData != null)
			{
				computer.setRAM(new RAM(JSONUtils.readProperty("name", ramData, "UNNAMED RAM"), JSONUtils.readProperty("speed_mhz", ramData, 1), JSONUtils.readProperty("size_mb", ramData, 2048)));
			}
			
			JSONObject diskData = JSONUtils.readObjectProperty("disk", computerData, null);
			if(cpuData != null)
			{
				computer.setDisk(new Disk(JSONUtils.readProperty("name", diskData, "UNNAMED DISK"), JSONUtils.readProperty("speed_mbps", diskData, 100), JSONUtils.readProperty("size_mb", diskData, 60000)));
			}
			
			JSONObject osData = JSONUtils.readObjectProperty("os", computerData, null);
			if(cpuData != null)
			{
				computer.installOS(new OperatingSystem(JSONUtils.readProperty("name", osData, "UNNAMED OS"), JSONUtils.readProperty("size_mb", osData, 10000)));
			}
			
			return computer;
		}
		
		return null;
	}
	
	public static void printPC(Computer pc)
	{
		System.out.println("PC Configuration:");
		System.out.println("\tCPU: " + pc.cpu().name() + " @ " + pc.cpu().speed() + "Mhz");
		System.out.println("\tRAM: " + pc.ram().name() + " (" + pc.ram().size() + "MB) @ " + pc.ram().speed() + "Mhz");
		System.out.println("\tDisk: " + pc.disk().name() + " (" + pc.disk().size() + "MB) @ " + pc.disk().speed() + "MBPS");
		System.out.println("\tOS: " + pc.os().name());
	}
	
	public static ArrayList<Software> setupSoftwareRepository(JSONObject fromData)
	{
		ArrayList<Software> theList = new ArrayList<>();
		JSONArray software = JSONUtils.readArrayProperty("software", fromData, null);
		if (software != null)
		{
			for (int i = 0; i < software.length(); i++)
			{
				try
				{
					theList.add(new Software(JSONUtils.readProperty("name", software.getJSONObject(i), "UNNAMED SOFTWARE"), JSONUtils.readProperty("size_mb", software.getJSONObject(i), 100)));
				}
				catch(JSONException e)
				{
				}
			}
		}
		return theList;
	}
	
	public static ArrayList<String> readCommands(JSONObject fromData)
	{
		ArrayList<String> commands = new ArrayList<>();
		JSONArray jCommands = JSONUtils.readArrayProperty("commands", fromData, null);
		for (int i = 0; i < jCommands.length(); i++)
		{
			try
			{
				commands.add(jCommands.getString(i));
			}
			catch (JSONException e)
			{
			}
		}
		return commands;
	}
	
	public static void runCommand(Computer onPc, ArrayList<Software> softRepo, String command)
	{
		String[] sCommand = command.split(" ");
		if (sCommand[0].equals("install"))
		{
			for (Software soft : softRepo)
			{
				if (soft.name().equals(sCommand[1]))
				{
					onPc.os().install(soft);
				}
			}
		}
		else if (sCommand[0].equals("uninstall"))
		{
			onPc.os().uninstall(sCommand[1]);
		}
		else if (sCommand[0].equals("launch"))
		{
			onPc.os().launch(sCommand[1]);
		}
		else if (sCommand[0].equals("close"))
		{
			onPc.os().close(sCommand[1]);
		}
	}
	
	public static void main(String[] args)
	{
		JSONObject data = loadDataFile(args[0]);
		Computer computer = setupPC(data);
		if (computer != null)
		{
			printPC(computer);
			ArrayList<Software> softRepo = setupSoftwareRepository(data);
			ArrayList<String> commands = readCommands(data);
			for (String command : commands)
			{
				runCommand(computer, softRepo, command);
			}
		}
	}
}
