package ar.edu.ub.testing;

public class CPU extends ComputerComponent
{
	public CPU(String name, int speedMhz)
	{
		super(name);
		this.m_speed = speedMhz;
	} 
	
	public int speed()
	{
		return this.m_speed;
	}
	
	private int m_speed;
}
