package ar.edu.ub.testing;

import java.util.ArrayList;

public class Disk extends ComputerComponent
{
	public Disk(String name, int speedMbps, int sizeMB)
	{
		super(name);
		this.m_speed = speedMbps;
		this.m_size = sizeMB;
		this.m_installedSoftware = new ArrayList<>();
	}
	
	public int speed()
	{
		return this.m_speed;
	}
	
	public int size()
	{
		return this.m_size;
	}
	
	public int taken()
	{
		int total = 0;
		for (Software soft : this.m_installedSoftware)
		{
			total += soft.size();
		}
		return total;
	}
	
	public boolean install(Software soft)
	{
		boolean canInstall = soft.size() < this.size() - this.taken();
		if (canInstall)
		{
			this.m_installedSoftware.add(soft);
			this.sleep(soft.size() / this.speed());
		}
		return canInstall;
	}
	
	public boolean uninstall(String softName)
	{
		for (Software soft : this.m_installedSoftware)
		{
			if (soft.name().equals(softName))
			{
				this.sleep(soft.size() / this.speed());
				this.m_installedSoftware.remove(soft);
				return true;
			}
		}
		
		this.sleep(2);
		return false;
	}
	
	public Software retrieve(String softName)
	{
		for (Software soft : this.m_installedSoftware)
		{
			if (soft.name().equals(softName))
			{
				this.sleep(soft.size() / this.speed());
				return soft;
			}
		}
		
		this.sleep(3);
		return null;
	}
	
	private int m_speed;
	private int m_size;
	private ArrayList<Software> m_installedSoftware;
}
