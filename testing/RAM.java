package ar.edu.ub.testing;

import java.util.ArrayList;

public class RAM extends ComputerComponent
{
	public RAM(String name, int speedMhz, int sizeMB)
	{
		super(name);
		this.m_speed = speedMhz;
		this.m_size = sizeMB;
		this.m_openSoftware = new ArrayList<>();
	}
	
	public int speed()
	{
		return this.m_speed;
	}
	
	public int size()
	{
		return this.m_size;
	}
	
	public int taken()
	{
		int total = 0;
		for (Software soft : this.m_openSoftware)
		{
			total += soft.size() / 10;
		}
		return total;
	}
	
	public boolean open(Software soft)
	{
		boolean canOpen = soft.size() / 10 < this.size() - this.taken();
		if (canOpen)
		{
			this.m_openSoftware.add(soft);
			this.sleep(soft.size() / this.speed());
		}
		return canOpen;
	}
	
	public boolean close(String softName)
	{
		for (Software soft : this.m_openSoftware)
		{
			if (soft.name().equals(softName))
			{
				this.sleep(soft.size() / this.speed());
				this.m_openSoftware.remove(soft);
				return true;
			}
		}
		
		this.sleep(1);
		return false;
	}
	
	private int m_speed;
	private int m_size;
	private ArrayList<Software> m_openSoftware;
}
