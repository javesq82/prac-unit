package ar.edu.ub.testing;

public abstract class ComputerComponent
{
	ComputerComponent(String name)
	{
		this.m_name = name;
		this.m_sleep = new SleepFunc()
		{
			
			@Override
			public void sleep(int seconds)
			{
				try
				{
					Thread.sleep(seconds * 1000);
				}
				catch (InterruptedException e)
				{
				}
			}
		};
	}
	
	public void sleep(int seconds)
	{
		if (this.m_sleep != null)
		{
			this.m_sleep.sleep(seconds);
		}
	}
	
	public String name()
	{
		return this.m_name;
	}
	
	private String m_name;
	private SleepFunc m_sleep;
}
