package ar.edu.ub.testing;

public class Computer
{
	public Computer()
	{
		this.m_cpu = null;
		this.m_ram = null;
		this.m_disk = null;
		this.m_os = null;
	}
	
	public void setCPU(CPU cpu)
	{
		this.m_cpu = cpu;
	}
	
	public void setRAM(RAM ram)
	{
		this.m_ram = ram;
	}
	
	public void setDisk(Disk disk)
	{
		this.m_disk = disk; 
	}
	
	public CPU cpu()
	{
		return this.m_cpu;
	}
	
	public RAM ram()
	{
		return this.m_ram;
	}
	
	public Disk disk()
	{
		return this.m_disk;
	}
	
	public OperatingSystem os()
	{
		return this.m_os;
	}
	
	public boolean installOS(OperatingSystem os)
	{
		os.output().print("Loading setup for " + os.name() + " into RAM");
		if (this.ram().open(os))
		{
			os.output().print("Installing " + os.name() + " into disk");
			if (this.disk().install(os))
			{
				this.ram().close(os.name());
				os.output().print("Successfully installed " + os.name());
				this.m_os = os;
				this.m_os.manage(this);
				return true;
			}
			else
			{
				this.ram().close(os.name());
				os.output().print("Not enough space in disk to install " + os.name());
			}
		}
		else
		{
			os.output().print("Not enough RAM to install " + os.name());
		}
		
		return false;
	}
	
	private CPU m_cpu;
	private RAM m_ram;
	private Disk m_disk;
	private OperatingSystem m_os;
}
