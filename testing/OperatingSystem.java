package ar.edu.ub.testing;

public class OperatingSystem extends Software
{
	public OperatingSystem(String name, int sizeMB)
	{
		super(name, sizeMB);
		this.m_output = new StandardOutput();
		this.m_computer = null;
	}
	
	public void setOuput(StandardOutput output)
	{
		this.m_output = output;
	}
	
	public StandardOutput output()
	{
		return this.m_output;
	}
	
	public void manage(Computer computer)
	{
		this.m_computer = computer;
	}
	
	public boolean install(Software soft)
	{
		this.output().print("Loading setup for " + soft.name() + " into RAM");
		if (this.m_computer.ram().open(soft))
		{
			this.output().print("Installing " + soft.name() + " into disk");
			if (this.m_computer.disk().install(soft))
			{
				this.m_computer.ram().close(soft.name());
				this.m_output.print("Successfully installed " + soft.name());
				return true;
			}
			else
			{
				this.m_computer.ram().close(soft.name());
				this.output().print("Not enough space in disk to install " + soft.name());
			}
		}
		else
		{
			this.output().print("Not enough RAM to install " + soft.name());
		}
		
		return false;
	}
	
	public boolean uninstall(String softName)
	{
		this.output().print("Attempting to uninstall " + softName);
		if (this.m_computer.disk().uninstall(softName))
		{
			this.output().print("Successfully uninstalled " + softName);
			return true;
		}
		return false;
	}
	
	public boolean launch(String softName)
	{
		this.output().print("Looking for an installation of " + softName);
		Software soft = this.m_computer.disk().retrieve(softName);
		if (soft != null)
		{
			this.output().print("Loading " + soft.name() + " into RAM");
			if (this.m_computer.ram().open(soft))
			{
				this.output().print("Application " + soft.name() + " launched!");
				return true;
			}
			else
			{
				this.output().print("Not enough RAM to launch" + soft.name());
			}
		}
		return false;
	}
	
	public boolean close(String softName)
	{
		this.output().print("Attempting to close " + softName);
		if (this.m_computer.ram().close(softName))
		{
			this.output().print(softName + " has been closed");
			return true;
		}
		this.output().print("Failed to unload " + softName + ", was not open");
		return false;
	}
	
	private StandardOutput m_output;
	private Computer m_computer; 
}
